#!/bin/sh

alarm() {
  ( \speaker-test --frequency $1 --test sine >/dev/null)& 
  pid=$!
  \sleep 0.${2}s
  \kill -9 $pid
}

check_availability()
{
	for url in $(cat "$2"); do
		domain=$(echo "${url}" | cut -d'.' -f2)
		pattern=$(grep "${domain}" patterns | cut -d'=' -f2)
		#echo "Verificar disponibilidade de "${1}" em ${domain} ... "
		if ! curl -s "${url}" | grep -iq "${pattern}"; then 
			echo "$1 disponível em $url" && alarm 400 200 && alarm 400 200 && alarm 400 200
		fi
	done
}
