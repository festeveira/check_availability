CHECK AVAILABILITY

Ficheiros
 - check_availability.sh
	
	Código.
	Dois argumentos:
	$1: Nome genérico do produto para os prints.
	$2: Ficheiro de URL's a precorrer.
	
	Faz download da página e procura pela PATTERN de indisponibilidade correspondente ao URL. (Ficheiro patterns)
	Se encontrar a PATTERN quer dizer que o produto está indisponível.
	Se não encontrar quer dizer que está disponível e dá print de uma mensagem e dá um aviso sonoro.		

 - check_gpu_availability.sh
	Tem duas chamadas ao código para as GPU's NVIDIA RX 3070 e AMD 6800X. (NVIDIA está comentado)
		
 - check_cpu_availability.sh
	Tem duas chamadas ao código para os CPU's AMD RYZEN 7 5800X e AMD RYZEN 9 5900X.

 - check_availability_test.sh
	Tem uma chamada ao código que percorre o ficheiro de URL's de teste. É suposto estarem disponíveis. Server para verificar se os alertas estão OK. Um URL por cada website.

 - check_loop.sh
	Corre check_cpu_availability.sh e check_gpu.availability.sh em loop infinito. (CPU está comentado)

 - patterns
	Contém as PATTERNS a procurar na página, dependendo do domínio. 

 - xxx_websites
	Contém as páginas dos produtos a procurar.

 ADICIONAR MAIS PRODUTOS

	Exemplo:
		Para adicionar mais um URL de uma AMD 6800X basta adicionar o URL no fim do ficheiro amd6800x_websites.
		Se o dominio ainda não existir nas patterns tem de se acrescentar a nova pattern.

		Para "www.pccomponentes.pt" é preciso acrescentar uma pattern "pccomponentes=PATTERN" ao ficheiro patterns.


 NOTAS

	Retirei 3 gráficas AMD do ficheiro que está a correr porque estão disponíveis mas são muito caras, assim não aparece alerta.
	Estão todos no ficheiro original amd6800x_websites_bck.

	AS VEZES HÁ FALSOS ALARMES PARA O SITE NOVOATALHO. Não sei porquê, não consegui corrigir.
